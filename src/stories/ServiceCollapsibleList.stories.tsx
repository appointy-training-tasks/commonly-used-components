import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import ServiceCollapsibleListComponent from "../components/ServiceCollapsibleList";
import { meetings } from "../shared/meetings";

export default {
  title: "Common Components/Service Collapsible List",
  component: ServiceCollapsibleListComponent,
} as ComponentMeta<typeof ServiceCollapsibleListComponent>;

const Template: ComponentStory<typeof ServiceCollapsibleListComponent> = (
  args
) => <ServiceCollapsibleListComponent {...args} />;

export const ServiceCollapsibleList = Template.bind({});
ServiceCollapsibleList.args = {
  meetings: meetings,
};
