import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import ReviewBoxComponent from "../components/ReviewBox";
import { reviews } from "../shared/reviews";

export default {
  title: "Common Components/Review Box",
  component: ReviewBoxComponent,
} as ComponentMeta<typeof ReviewBoxComponent>;

const Template: ComponentStory<typeof ReviewBoxComponent> = (args) => (
  <ReviewBoxComponent {...args} />
);

export const ReviewBox = Template.bind({});
ReviewBox.args = {
  title: "Bengaluru",
  reviews: reviews,
};
