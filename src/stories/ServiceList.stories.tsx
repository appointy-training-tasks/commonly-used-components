import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import ServiceListComponent from "../components/ServiceList";
import { services } from "../shared/services";

export default {
  title: "Common Components/Service List",
  component: ServiceListComponent,
} as ComponentMeta<typeof ServiceListComponent>;

const Template: ComponentStory<typeof ServiceListComponent> = (args) => (
  <ServiceListComponent {...args} />
);

export const ServiceList = Template.bind({});
ServiceList.args = {
  title: "SELECT SERVICE",
  services: services,
};
