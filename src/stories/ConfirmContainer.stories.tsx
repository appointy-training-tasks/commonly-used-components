import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import ConfirmContainerComponent from "../components/ConfirmContainer";

export default {
  title: "Common Components/Confirm Container",
  component: ConfirmContainerComponent,
} as ComponentMeta<typeof ConfirmContainerComponent>;

const Template: ComponentStory<typeof ConfirmContainerComponent> = (args) => (
  <ConfirmContainerComponent {...args} />
);

export const ConfirmContainer = Template.bind({});
ConfirmContainer.args = {
  open: true,
};
