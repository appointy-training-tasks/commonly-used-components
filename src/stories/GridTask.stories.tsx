import React from "react";
import { ComponentMeta, ComponentStory } from "@storybook/react";
import GridTaskComponent from "../components/GridTask";
import { Typography } from "@material-ui/core";
import { CreditCardOutlined } from "@material-ui/icons";

export default {
  title: "Common Components/Grid Task",
  component: GridTaskComponent,
} as ComponentMeta<typeof GridTaskComponent>;

const Template: ComponentStory<typeof GridTaskComponent> = (args) => (
  <GridTaskComponent {...args} />
);

const PaymentTask = () => (
  <Typography style={{ display: "flex", alignItems: "center", gap: "5px" }}>
    <CreditCardOutlined fontSize="small" />
    Requires Credit Card
  </Typography>
);
export const GridTask = Template.bind({});
GridTask.args = {
  children: ["15m", "15m", <PaymentTask />, <PaymentTask />],
};
