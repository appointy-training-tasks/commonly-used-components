import Review from "../types/review";

export const reviews: Review[] = [
  { review: "Overall", stars: 3 },
  { review: "Ease-of-use", stars: 3 },
  { review: "Features", stars: 3 },
  { review: "Look & Feel", stars: 4 },
];
