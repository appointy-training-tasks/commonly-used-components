import Meeting from "../types/meeting";

export const meetings: Meeting[] = [
  {
    id: 0,
    title: "Bridal Services",
    services: [
      {
        id: 0,
        title: "Hair Cut",
        details: [
          "30m - 60m",
          "Starting from 999.00",
          "Served at customer location",
          "Group Service",
        ],
        conditions: "Requires pre-payment",
      },
      {
        id: 2,
        title: "Bridal Makeup",
        details: ["30m", "3001.00", "Online service"],
        conditions: "Requires pre-payment",
      },
      {
        id: 3,
        title: "Ready For Garba",
        details: ["30m", "499.0", "At business location"],
        conditions: "Requires pre-payment",
      },
    ],
  },
  {
    id: 1,
    title: "Default",
    services: [
      {
        id: 0,
        title: "Default",
        details: ["Default service"],
      },
    ],
  },
];
