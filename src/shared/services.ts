import Service from "../types/service";

export const services: Service[] = [
  { id: 1, title: "Hair Cut", details: ["30m", "₹799.00"] },
  { id: 2, title: "Hair Color", details: ["45m", "₹1299.00"] },
  {
    id: 3,
    title: "Hair Spa",
    details: ["120m", "₹1599.00", "Group Service"],
  },
];
