import {
  Avatar,
  Box,
  Button,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { CreditCardOutlined } from "@material-ui/icons";
import clsx from "clsx";
import React, { useState } from "react";
import Service from "../types/service";
import GridTask from "./GridTask";

interface ServiceListProps {
  title?: string;
  services: Service[];
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4),
  },
  title: {
    marginLeft: theme.spacing(12),
    fontSize: "1rem",
    marginBottom: theme.spacing(4),
  },
  conditions: {
    color: theme.palette.text.secondary,
    display: "flex",
    alignItems: "center",
    gap: "5px",
    marginTop: "12px",
  },
}));

const ServiceList: React.FC<ServiceListProps> = ({ title, services }) => {
  const [hoverId, setHoverId] = useState<number | null>(null);
  const classes = useStyles();

  const onClick = (id: number | null) => {
    if (hoverId === id) setHoverId(null);
    else setHoverId(id);
  };

  return (
    <Box className={clsx({ [classes.root]: !!title })}>
      {title && (
        <Typography variant="h5" className={classes.title}>
          {title}
        </Typography>
      )}
      <List>
        {services.map(({ id, title, details, conditions }, index) => (
          <Box key={id}>
            <ListItem
              button
              onClick={() => onClick(id)}
              style={{ alignItems: "flex-start" }}>
              <ListItemAvatar>
                <Avatar
                  style={{ marginTop: "5px" }}
                  src={`https://ui-avatars.com/api/?name=${title}&background=random&length=1`}
                />
              </ListItemAvatar>
              <ListItemText
                primary={title}
                secondary={
                  <>
                    <GridTask>{details}</GridTask>
                    {conditions && (
                      <Typography className={classes.conditions}>
                        <CreditCardOutlined fontSize="small" />
                        {conditions}
                      </Typography>
                    )}
                  </>
                }
              />
              {hoverId === id && (
                <ListItemSecondaryAction>
                  <Button variant="outlined" color="primary">
                    SELECT
                  </Button>
                </ListItemSecondaryAction>
              )}
            </ListItem>
            {index < services.length - 1 && <Divider />}
          </Box>
        ))}
      </List>
    </Box>
  );
};

export default ServiceList;
