import { Grid, makeStyles } from "@material-ui/core";
import { FiberManualRecord } from "@material-ui/icons";
import React from "react";

type GridTaskProps = {
  children: React.ReactNode;
};

const useStyles = makeStyles((theme) => ({
  grid: {
    color: theme.palette.text.secondary,
  },
  delimiter: {
    fontSize: 8,
    color: theme.palette.text.disabled,
  },
}));

const GridTask: React.FC<GridTaskProps> = ({ children }) => {
  const classes = useStyles();

  return (
    <Grid
      container
      direction="row"
      spacing={2}
      className={classes.grid}
      alignItems="center">
      {React.Children.map<React.ReactNode, React.ReactNode>(
        children,
        (child) => <Grid item>{child}</Grid>
      )?.reduce((previous, current) => [
        previous,
        <Grid item aria-hidden>
          <FiberManualRecord className={classes.delimiter} />
        </Grid>,
        current,
      ]) || ""}
    </Grid>
  );
};

export default GridTask;
