import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import React from "react";

interface ConfirmContainerProps {
  open: boolean;
  onCancel: () => void;
  onConfirm: () => void;
}

const ConfirmContainer: React.FC<ConfirmContainerProps> = ({
  open,
  onCancel,
  onConfirm,
}) => {
  return (
    <Dialog open={open} fullWidth>
      <DialogTitle>Delete Service</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Are you sure you want to continue?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel}>CANCEL</Button>
        <Button variant="contained" color="primary" onClick={onConfirm}>
          CONFIRM
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmContainer;
