import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import React from "react";
import Review from "../types/review";
import Rating from "@material-ui/lab/Rating";
import { FormatQuoteOutlined } from "@material-ui/icons";
import clsx from "clsx";

interface ReviewBoxProps {
  title: string;
  reviews: Review[];
}

const useStyles = makeStyles((theme) => ({
  card: { padding: "0 5px", width: "500px" },
  title: {
    marginLeft: theme.spacing(2),
  },
  ratingBox: {
    margin: `${theme.spacing(4)}px ${theme.spacing(8)}px`,
  },
  ratingStarsText: {
    marginLeft: theme.spacing(1),
  },
  quote: {
    marginBottom: theme.spacing(2),
  },
  quoteInverted: {
    transform: "scaleX(-1) scaleY(-1)",
  },
}));

const ReviewBox: React.FC<ReviewBoxProps> = ({ title, reviews }) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar
            alt="Bengaluru"
            src={`https://ui-avatars.com/api/?name=${title}&background=random&length=2`}
          />
        }
        title={
          <Typography variant="h5" component="h3">
            {title}
          </Typography>
        }
      />
      <CardContent>
        <Typography variant="body1" className={classes.title}>
          Please rate your last experience
        </Typography>
        <Box className={classes.ratingBox}>
          <Grid container justifyContent="center">
            {reviews.map((review) => (
              <Grid container item direction="row" alignItems="center">
                <Grid item xs={6}>
                  <Typography>{review.review}</Typography>
                </Grid>
                <Grid container item xs={6} direction="row" alignItems="center">
                  <Rating value={review.stars} size="large" readOnly />
                  <Box className={classes.ratingStarsText}>
                    <Typography>{review.stars}</Typography>
                  </Box>
                </Grid>
              </Grid>
            ))}
          </Grid>
        </Box>
        <Box>
          <Grid
            container
            direction="row"
            spacing={4}
            alignItems="center"
            justifyContent="center">
            <Grid item>
              <FormatQuoteOutlined
                className={clsx(classes.quote, classes.quoteInverted)}
              />
            </Grid>
            <Grid item>
              <Typography>Thanks!</Typography>
            </Grid>
            <Grid item>
              <FormatQuoteOutlined className={classes.quote} />
            </Grid>
          </Grid>
        </Box>
      </CardContent>
      <CardActions>
        <Grid container justifyContent="flex-end">
          <Button variant="contained" color="secondary">
            EDIT
          </Button>
        </Grid>
      </CardActions>
    </Card>
  );
};

export default ReviewBox;
