import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Avatar,
  Box,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { ExpandMoreRounded } from "@material-ui/icons";
import React from "react";
import Meeting from "../types/meeting";
import ServiceList from "./ServiceList";

interface ServiceCollapsibleListProps {
  meetings: Meeting[];
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(4),
    marginBotton: theme.spacing(15),
  },
  title: {
    marginLeft: theme.spacing(12),
    fontSize: "1rem",
    marginBottom: theme.spacing(4),
  },
  accordion: {
    boxShadow: "none",

    "&::before": {
      background: "none",
    },
  },
  accordionTitle: {
    color: theme.palette.text.secondary,
    textTransform: "uppercase",
    marginLeft: theme.spacing(4),
  },
}));

const ServiceCollapsibleList: React.FC<ServiceCollapsibleListProps> = ({
  meetings,
}) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Typography variant="h5" component="h2" className={classes.title}>
        SELECT MEETING TYPE
      </Typography>
      {meetings.map(({ id, title, services }) => (
        <Accordion key={`a-${id}`} className={classes.accordion}>
          <AccordionSummary expandIcon={<ExpandMoreRounded />}>
            <Grid container alignItems="center">
              <Grid item>
                <Avatar
                  alt={title}
                  src={`https://ui-avatars.com/api/?name=${title}&background=random&length=1`}
                />
              </Grid>
              <Grid item>
                <Typography className={classes.accordionTitle}>
                  {title}
                </Typography>
              </Grid>
            </Grid>
          </AccordionSummary>
          <AccordionDetails>
            <Box style={{ flex: 1 }}>
              <ServiceList services={services} />
            </Box>
          </AccordionDetails>
        </Accordion>
      ))}
    </Box>
  );
};

export default ServiceCollapsibleList;
