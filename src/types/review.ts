export default interface Review {
  review: string;
  stars: number;
}
