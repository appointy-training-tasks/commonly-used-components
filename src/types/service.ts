export default interface Service {
  id: number;
  title: string;
  details: string[];
  conditions?: string;
}
