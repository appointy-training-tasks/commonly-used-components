import Service from "./service";

export default interface Meeting {
  id: number;
  title: string;
  services: Service[];
}
